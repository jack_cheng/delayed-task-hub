package api

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"delayed-task-hub/common/errorx"
	"github.com/go-playground/validator"

	"delayed-task-hub/internal/logic/api"
	"delayed-task-hub/internal/svc"
	"delayed-task-hub/internal/types"
)

func Create_taskHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.CreateTaskReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		l := api.NewCreate_taskLogic(r.Context(), svcCtx)
		resp, err := l.Create_task(&req)
		if err != nil {
			if _, ok := err.(*errorx.CodeError); ok {
				httpx.OkJson(w, err)
			} else {
				httpx.Error(w, err)
			}
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
