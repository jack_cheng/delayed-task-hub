package config

import (
	"github.com/zeromicro/go-queue/dq"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/rest"
)

type Config struct {
	rest.RestConf
	Redis struct {
		Address string
		Pass    string
	}
	DqConf  dq.DqConf
	LogConf logx.LogConf
}
