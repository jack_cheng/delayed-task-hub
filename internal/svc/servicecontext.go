package svc

import (
	"delayed-task-hub/internal/config"
	"github.com/zeromicro/go-queue/dq"
	"github.com/zeromicro/go-zero/core/stores/redis"
)

type ServiceContext struct {
	Config         config.Config
	DqPusherClient dq.Producer
	Redis          *redis.Redis
	//DqConsumer     dq.Consumer
}

func NewServiceContext(c config.Config) *ServiceContext {
	newRedis := redis.New(c.Redis.Address, redisConfig(c))

	return &ServiceContext{
		Config:         c,
		DqPusherClient: dq.NewProducer(c.DqConf.Beanstalks),
		Redis:          newRedis,
		//DqConsumer:     consumer,
	}
}

func redisConfig(c config.Config) redis.Option {
	return func(r *redis.Redis) {
		r.Type = redis.NodeType
		r.Pass = c.Redis.Pass
	}
}
