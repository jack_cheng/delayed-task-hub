package api

import (
	"context"
	"delayed-task-hub/common"
	"delayed-task-hub/common/uniqueid"
	"encoding/json"
	"time"

	"delayed-task-hub/internal/svc"
	"delayed-task-hub/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type Create_task_atLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewCreate_task_atLogic(ctx context.Context, svcCtx *svc.ServiceContext) *Create_task_atLogic {
	return &Create_task_atLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *Create_task_atLogic) Create_task_at(req *types.CreateTaskAtReq) (resp *types.CreateTaskResp, err error) {
	resp = &types.CreateTaskResp{}
	index := uniqueid.GenId()
	logx.Infof("index=%v", index)
	task := common.Package{
		ListName: req.ListName,
		PayLoad:  req.PayLoad,
		Index:    index,
	}

	taskJson, err := json.Marshal(task)
	if err != nil {
		resp.Code = 1
		resp.Msg = "格式错误"
		return
	} else {
		_, err := l.svcCtx.DqPusherClient.At(taskJson, time.Unix(req.TimeStamp, 0))
		if err != nil {
			logx.Errorf("error from DqPusherClient Delay err : %v", err)
		}
	}
	return
}
