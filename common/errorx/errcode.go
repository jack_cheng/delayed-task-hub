package errorx

import "google.golang.org/grpc/codes"

const OK uint32 = 0

// 系统内部未知错误
const INTERNAL_ERROR = 1
const INTERNAL_ERROR_500 = 1

// admin user错误
const ADMIN_ERROR = 9000

// 用户
const USER_ERROR = 10000

// 好友(作废)
const FRIEND_ERROR = 11000

// 签到(作废)
const CHECKIN_ERROR = 12000

// 奖励作废
const REWARD_ERROR = 13000

// 公告
const NOTICE_ERROR = 14000

// 配置中心
const CONFIG_CENTER_ERROR = 15000

// 角色
const ROLE_ERROR = 16000

// 邮件
const MAILBOX_ERROR = 17000

// 服务器区服
const SERVER_ERROR = 18000

var ERROR_MAP = make(map[int]string)

const (
	UserNotExist codes.Code = 1001
)

func init() {
	ERROR_MAP[3] = "参数错误"
	ERROR_MAP[401] = "权限不足禁止访问"
	ERROR_MAP[INTERNAL_ERROR] = "系统内部错误"
	ERROR_MAP[INTERNAL_ERROR_500] = "系统内部错误"

	ERROR_MAP[ADMIN_ERROR+1] = "账号或者密码错误"
	//用户模块
	ERROR_MAP[USER_ERROR+1] = "账号或者密码错误"
	ERROR_MAP[USER_ERROR+2] = "参数错误"
	ERROR_MAP[USER_ERROR+3] = "手机号已注册"
	ERROR_MAP[USER_ERROR+4] = "邮箱已注册"
	ERROR_MAP[USER_ERROR+5] = "用户不存在"
	ERROR_MAP[USER_ERROR+6] = "原密码错误"
	ERROR_MAP[USER_ERROR+7] = "账号或者密码错误"
	ERROR_MAP[USER_ERROR+8] = "当前账号可能在别的终端登录"
	ERROR_MAP[USER_ERROR+9] = "手机号格式错误"
	ERROR_MAP[USER_ERROR+10] = "原密码不能跟现在密码一样"
	ERROR_MAP[USER_ERROR+11] = "身份证格式错误"
	ERROR_MAP[USER_ERROR+12] = "未成年人不能绑定"
	//好友相关
	ERROR_MAP[FRIEND_ERROR+1] = "好有申请不存在"
	ERROR_MAP[FRIEND_ERROR+2] = "已经是好友了不能重复申请"

	//签到
	ERROR_MAP[CHECKIN_ERROR+1] = "发生错误签到失败"
	ERROR_MAP[CHECKIN_ERROR+2] = "不能重复签到"

	//	奖励
	ERROR_MAP[REWARD_ERROR+1] = "创建奖励错误"
	// 公告
	ERROR_MAP[NOTICE_ERROR+1] = "没有公告"
	//配置中心问题
	ERROR_MAP[CONFIG_CENTER_ERROR+1] = "保存配置错误"
	ERROR_MAP[CONFIG_CENTER_ERROR+2] = "删除原有的配置失败"
	ERROR_MAP[CONFIG_CENTER_ERROR+3] = "配置文件格式错误"
	//	角色
	ERROR_MAP[ROLE_ERROR+1] = "角色数据为空"
	ERROR_MAP[ROLE_ERROR+2] = "没有找到角色"
	ERROR_MAP[ROLE_ERROR+3] = "角色ID不能跟原角色ID重复"
	ERROR_MAP[ROLE_ERROR+4] = "角色ID重复"
	ERROR_MAP[ROLE_ERROR+5] = "更新角色ID失败"
	//	公告
	ERROR_MAP[NOTICE_ERROR+1] = "没有找到公告"
	//	邮件系统
	ERROR_MAP[MAILBOX_ERROR+1] = "保存邮件错误"
	ERROR_MAP[MAILBOX_ERROR+2] = "搜索邮件错误"
	ERROR_MAP[MAILBOX_ERROR+3] = "没有找邮件"
	// 区服
	ERROR_MAP[SERVER_ERROR+1] = "没有区服"

}
