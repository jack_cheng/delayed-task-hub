package utils

import (
	"delayed-task-hub/common/uniqueid"
	"fmt"
	"testing"
	"time"
)

type nextcrontimes struct {
	from string
	next string
	prev string
}

type nextcrontest struct {
	expr   string
	layout string
	times  []nextcrontimes
}

var crontests = []nextcrontest{
	//周一和周三 早上8点开始新的一期排行榜
	{
		"1 8 * * 1,3",
		"2006-01-02 15:04:05",
		[]nextcrontimes{
			{"2017-03-06 00:00:00", "2017-03-06 08:01:00", "2017-03-01 08:01:00"},
			{"2017-03-06 08:02:00", "2017-03-08 08:01:00", "2017-03-06 08:01:00"},
		},
	},
	// 每月1号0点开始一个排行榜
	{
		"0 0 1 * *",
		"2006-01-02 15:04:05",
		[]nextcrontimes{
			{"2017-03-06 00:00:00", "2017-04-01 00:00:00", "2017-03-01 00:00:00"},
			{"2017-03-31 00:01:00", "2017-04-01 00:00:00", "2017-03-01 00:00:00"},
		},
	},
	//每周一开始早上8点
	{
		"1 8 * * 1",
		"2006-01-02 15:04:05",
		[]nextcrontimes{
			{"2017-03-06 00:00:00", "2017-03-06 08:01:00", "2017-02-27 08:01:00"},
			{"2017-03-06 09:00:00", "2017-03-13 08:01:00", "2017-03-06 08:01:00"},
			{"2017-03-27 09:00:00", "2017-04-03 08:01:00", "2017-03-27 08:01:00"},
		},
	},
}

func TestNextPrev(t *testing.T) {
	for _, test := range crontests {
		for _, times := range test.times {
			from, _ := time.Parse("2006-01-02 15:04:05", times.from)
			next := NextCronTime(test.expr, from)
			prev := PrevCronTime(test.expr, from)

			nextstr := next.Format(test.layout)
			prevstr := prev.Format(test.layout)

			fmt.Println("form=", from)
			fmt.Println("test.layout=", test.layout)
			fmt.Println("next=", next)
			fmt.Println("nextstr=", nextstr)
			fmt.Println("times.next=", times.next)

			fmt.Println("prev=", prev)
			fmt.Println("prevstr=", prevstr)
			fmt.Println("times.prev=", times.prev)
			if nextstr != times.next {
				t.Errorf(`("%s").Next("%s") = "%s", got "%s"`, test.expr, times.from, times.next, nextstr)
			}

			if prevstr != times.prev {
				t.Errorf(`("%s").Prev("%s") = "%s", got "%s"`, test.expr, times.from, times.prev, prevstr)
			}
		}
	}
}

func TestObfuscate(t *testing.T) {
	origID := uniqueid.GenId()
	obfuscatedID := Obfuscate(uint64(origID))
	deobfuscatedID := Deobfuscate(obfuscatedID)
	if deobfuscatedID != deobfuscatedID {
		t.Errorf(" expected=%v, got = %v", origID, deobfuscatedID)
	}
}
