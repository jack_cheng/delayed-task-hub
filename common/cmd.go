package common

type Package struct {
	ListName string `json:"list_name"`
	PayLoad  string `json:"pay_load"`
	Index    int64  `json:"index"`
}
