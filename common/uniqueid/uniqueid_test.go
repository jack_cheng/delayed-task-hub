package uniqueid

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenID(t *testing.T) {
	id := GenId()
	fmt.Println("id: ", id)
	assert.Equal(t, id > 0, true)
}
