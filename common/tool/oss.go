package tool

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash"
	"io"
	"path/filepath"
	"time"
)

type OSSConfig struct {
	AccessKeyId     string
	AccessKeySecret string
	EndPoint        string
	CallbackUrl     string
}

var c OSSConfig

type ConfigStruct struct {
	Expiration string     `json:"expiration"`
	Conditions [][]string `json:"conditions"`
}

type CallbackParam struct {
	CallbackUrl      string `json:"callbackUrl"`
	CallbackBody     string `json:"callbackBody"`
	CallbackBodyType string `json:"callbackBodyType"`
}

type PolicyToken struct {
	AccessKeyId string `json:"accessid"`
	Host        string `json:"host"`
	Expire      int64  `json:"expire"`
	Signature   string `json:"signature"`
	Policy      string `json:"policy"`
	Directory   string `json:"dir"`
	Callback    string `json:"callback"`
}

func get_gmt_iso8601(expire_end int64) string {
	var tokenExpire = time.Unix(expire_end, 0).UTC().Format("2006-01-02T15:04:05Z")
	return tokenExpire
}

func SetOSSUp(endPoint, accessKeyId, accessKeySecret, callbackUrl string) {
	c.EndPoint = endPoint
	c.AccessKeyId = accessKeyId
	c.AccessKeySecret = accessKeySecret
	c.CallbackUrl = callbackUrl
}

func GetPolicyToken(bucketName, dirName, fileName string, expireTime int64, resId string) PolicyToken {
	now := time.Now().Unix()
	expire_end := now + expireTime
	var tokenExpire = get_gmt_iso8601(expire_end)

	//create post policy json
	var config ConfigStruct
	config.Expiration = tokenExpire
	var condition []string
	condition = append(condition, "starts-with")
	condition = append(condition, "$key")
	condition = append(condition, filepath.Join(dirName, fileName))
	config.Conditions = append(config.Conditions, condition)

	//calucate signature
	result, err := json.Marshal(config)
	debyte := base64.StdEncoding.EncodeToString(result)
	h := hmac.New(func() hash.Hash { return sha1.New() }, []byte(c.AccessKeySecret))
	io.WriteString(h, debyte)
	signedStr := base64.StdEncoding.EncodeToString(h.Sum(nil))

	var callbackParam CallbackParam
	callbackParam.CallbackUrl = c.CallbackUrl
	callbackParam.CallbackBody = "filename=${object}&size=${size}&mimeType=${mimeType}&height=${imageInfo.height}&width=${imageInfo.width}&id=" + resId
	callbackParam.CallbackBodyType = "application/x-www-form-urlencoded"

	callback_str, err := json.Marshal(callbackParam)
	if err != nil {
		fmt.Println("callback json err:", err)
	}
	callbackBase64 := base64.StdEncoding.EncodeToString(callback_str)

	var policyToken PolicyToken
	policyToken.AccessKeyId = c.AccessKeyId
	policyToken.Host = fmt.Sprintf("http://%v.%v.aliyuncs.com", bucketName, c.EndPoint)
	policyToken.Expire = expire_end
	policyToken.Signature = string(signedStr)
	policyToken.Directory = dirName
	policyToken.Policy = string(debyte)
	policyToken.Callback = string(callbackBase64)
	// response, err := json.Marshal(policyToken)
	// if err != nil {
	// 	fmt.Println("json err:", err)
	// }
	// return string(response)
	return policyToken
}
