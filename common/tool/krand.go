package tool

import (
	"fmt"
	"github.com/go-redis/redis/v7"
	"math/rand"
	"sync"
	"time"
)

const (
	KC_RAND_KIND_NUM   = 0 // 纯数字
	KC_RAND_KIND_LOWER = 1 // 小写字母
	KC_RAND_KIND_UPPER = 2 // 大写字母
	KC_RAND_KIND_ALL   = 3 // 数字、大小写字母
)

// 随机字符串
func Krand(size int, kind int) string {
	ikind, kinds, result := kind, [][]int{{10, 48}, {26, 97}, {26, 65}}, make([]byte, size)
	is_all := kind > 2 || kind < 0
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < size; i++ {
		if is_all { // random ikind
			ikind = rand.Intn(3)
		}
		scope, base := kinds[ikind][0], kinds[ikind][1]
		result[i] = uint8(base + rand.Intn(scope))
	}
	return string(result)
}

var lastTime int64
var msgIndex int

func MakeMessageID() int64 {
	t := time.Now().Unix()
	if lastTime != t {
		lastTime = t
		msgIndex = 0
	}
	msgIndex++
	n := t << 16
	index := int64(msgIndex & 0xff)
	return n | index
}

var (
	prefixes = []string{
		"焰火", "紫雨", "碧海", "赤霸", "冰", "死神", "黑暗",
		"辉煌", "迷失", "疾风", "不灭", "轻盈", "潜行", "炽热",
		"虚无", "断罪", "神秘", "神圣", "秘银", "逍遥", "镜花",
		"破晓", "酒痴", "刀锋", "雪花", "蓝月", "如意", "百战",
		"火龙", "雷鸣", "月影", "紫电", "晓星", "寒霜", "烈火",
		"厄运", "荒野", "轮回", "苍穹", "噬魂", "流星", "黎明",
		"天雷", "落叶", "裂空", "风云", "冥", "星辰", "梦幻",
		"铁血", "狂龙", "月夜", "湛蓝", "混沌", "幻像", "天籁",
		"玉兰", "绿竹", "琉璃", "霜华", "风铃", "残阳", "混沌",
		"流沙", "黄沙", "天蓝", "战意", "灭世", "绝望", "苍生",
		"独孤", "银羽", "荣耀", "飞花", "霹雳", "疾风", "烽火",
		"星尘", "追梦", "雄心", "隐匿", "天启", "幽冥", "樱花",
		"霸者", "破灭", "黑炎", "鹰眼", "羽翼", "剑雨", "苍穹",
		"战争", "斩杀", "血月", "封神", "骁勇", "妖姬", "雪鹰",
		"海潮", "寂静", "诛神", "泣血", "冰魄", "风暴",
	}
	nouns = []string{
		"", "孤狼", "九天", "覇者", "使者", "狂刀", "旅人", "傲天", "千叶", "雾影",
		"千杀", "无面", "亡者", "幻灵", "风暴", "炼狱", "空罪", "圣者", "墓人", "烈火",
		"刺客", "剑圣", "天极", "影者", "狂斩", "杀神", "夜幕", "阿努比斯", "无尽", "卡西奥",
		"比尔吉沃特", "德莱厄斯", "飞斩", "泰坦", "独眼巨人", "兵临城下", "虚空", "蜘蛛", "蓝斑", "派克",
		"雷克塞", "酒桶", "死影", "疾风", "黑暗之女", "织影", "元首", "时间", "黑骑", "战争之王",
		"邪恶小法师", "薇恩", "猩红收割者", "虚神", "洛", "阿卡丽", "无情战士", "永猎双子", "暴怒骑士", "放逐之刃",
		"雷霆咆哮", "恶魔小丑", "虚空遁地兽", "木木", "石像鬼", "复仇焰魂", "扭曲树精", "冰晶凤凰", "远古巫灵", "皎月女神",
		"暮光星灵", "流浪法师", "德玛西亚皇子", "邪恶之锤", "死亡颂唱者", "兽灵行者", "寒冰射手", "末日使者", "海洋之灾", "海兽祭司",
		"盲僧", "荆棘之兴", "酒桶", "熔岩巨兽", "诺克萨斯之手", "龙血武姬", "殇之木乃伊", "水晶先锋", "寡妇制造者", "魔蛇之拥",
	}
	suffixes = []string{"之怒", "之心", "之影", "之瞳", "之舞", "之墓", "", "之力", "之魂", "之刃", "之爱", "之翼", "之友", "之王", "之刺", "之幻", "之秘", "之泣", "之山", "之境"}
)

func GenerateChineseGameName() string {
	rand.Seed(time.Now().UnixNano())
	prefix := prefixes[rand.Intn(len(prefixes))]
	noun := nouns[rand.Intn(len(nouns))]
	suffix := suffixes[rand.Intn(len(suffixes))]
	return prefix + noun + suffix
}

var lettersAndNumbers = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func RandomString(length int, seed int64) string {
	randGen := rand.New(rand.NewSource(seed)) // 创建带有指定种子的随机数生成器
	b := make([]rune, length)
	for i := 0; i < length; i++ {
		b[i] = lettersAndNumbers[randGen.Intn(len(lettersAndNumbers))]
	}
	return string(b)
}

var (
	src = rand.NewSource(time.Now().UnixNano())
	gen = rand.New(src)
	mtx = &sync.Mutex{}
)

func GenerateUniqueNumber(app string, client *redis.Client, key string) int {
	mtx.Lock()
	num := gen.Intn(90000000) + 10000000
	mtx.Unlock()

	for {
		//if result, err := client.SAdd(key, num).Result(); err == nil && result > 0 {
		//	return num
		//}
		result, err := client.SAdd(fmt.Sprintf("%v:%v", app, key), num).Result()
		if err == nil && result > 0 {
			return num
		}

		mtx.Lock()
		num = gen.Intn(90000000) + 10000000
		mtx.Unlock()
	}
}
