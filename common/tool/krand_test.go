package tool

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

// 测试生成的key全是数字
func TestKrand1(t *testing.T) {
	s1 := Krand(10, 0)
	assert.Equal(t, len(s1), 10)
	_, err := strconv.Atoi(s1)
	assert.Equal(t, err, nil)
}

// 测试生成的key全是字母
func TestKrand2(t *testing.T) {
	s1 := Krand(10, 1)
	s2 := Krand(10, 2)
	assert.Equal(t, len(s1), 10)

	b1 := IsStrAllLowerLetter(s1)
	assert.Equal(t, b1, true)
	b2 := IsStrAllCaptialLetter(s2)
	assert.Equal(t, b2, true)
}

// 测试生成数字和字母的混合字符串
func TestKrand3(t *testing.T) {
	s1 := Krand(20, 3)
	assert.Equal(t, len(s1), 20)

	b := IsStrAllNumberAndLetter(s1)
	assert.Equal(t, b, true)
}
