.PHONY: all build clean run check cover lint docker help

BIN_FILE=xmjy_api
BUILD_TIME=`date +%Y%m%d.%H%M`

build:
	@CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o "${BIN_FILE}"

run:
	@goctl api go -api ./api/api.api -dir . -style gozero --home $(PWD)/goctl_tpl
	@go run delaytask.go -f etc/delaytask.yaml

gen:
	@goctl api go -api ./api/api.api -dir . -style gozero --home $(PWD)/goctl_tpl

doc:
	@goctl api doc --dir ./api/api.api  --o .

docker:
	if [ -z "${ver}" ]; then echo "请输入版本号"; fi;
	@echo "镜像打包完成 版本为: delayed-task-hub:${ver}"
	# if [ -a Dockerfile ]; then rm Dockerfile; fi;
	# @goctl docker --go armvp.go --port 8888
	@cd $(PWD)/../ && pwd &&  docker build -t delayed-task-hub:${ver} -f app/Dockerfile .
	@echo "镜像打包完成 版本为: delayed-task-hub:${ver}"

swagger:
	@goctl api plugin -plugin goctl-swagger="swagger -filename delayedtask.json -host 106.14.221.92:8888" -api ./api/api.api -dir .
help:
	@echo "make 格式化go代码 并编译生成二进制文件"
	@echo "make build 编译go代码生成二进制文件"
	@echo "make clean 清理中间目标文件"
	@echo "make dbmodel 生成数据库模型"
	@echo "make doc 生成文档"
	@echo "make test 执行测试case"
	@echo "make check 格式化go代码"
	@echo "make cover 检查测试覆盖率"
	@echo "make run 直接运行程序"
	@echo "make lint 执行代码检查"
	@echo "make docker 构建docker镜像"
